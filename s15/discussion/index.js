

//Single Line Comment

/* Multi Comments
This is a Comment
*/

// [Section] Syntax, Statements and Comments
// Statements in programming, these are the instructions that we tell the computer to perform

//JavaScript Statement usually it ends with semicolon(;)
//Semi colons are not required in JS, but we will use it to help us prepare for the other strict language live java

//A syntax in programming , it it the set of rules that describes how statements must be constructed

//All line or block of codes should be written in specific manner or ell the statement will not run


//[Section] Variables
//it used to contain/store data
//any info that is used by an application it stored in what we call memory
//When we craete variables certain portion of device memory is given a name that we call "variables"

//Declaring variables
//Declaring variables it tells our devices that a variable name is created and is ready to store data
//Syntax:
//let/const variableName

let myVariable;

//if you declare a variable and did not initialize its value it will become undefine
//console.log is useful by printing values of variable or certain result of code into the google chrom browser

console.log(myVariable);

/*

		Guides in writing variables
			1.use the let keyword followed by the variable name of your choice and use the assignment operator meeans ='
			to assign its value'
			2.variable name should start with a lower case character, use camelCase for multiple words
			3.For constant variables, use the const keyword
			4.Variable names it should be indecative(descriptive)of the value being stiored to avoid confusion */
//Declare and initialize variables	
//Initializing variables the instance when a variable is given it initial or starting value
//Syntax of declaration
//let/const variableName = value;

//exaMPLE: 

let productName = 'desktop computer';
console.log(productName);		

let productPrice = 18999;
console.log(productPrice);

//In the context of certain application, some variables or information are constant and should not change
//in this example, interest rate for a loan, savings account or motgage much not change due to real world concenrs

const interest = 3.539;	

//Reassigning varialble values
//Reassigning a variable it means changing its initial or previous value into another value
		//Syntax 
		//variableName = newValue

productName = 'Laptop';
console.log(productName);

//The valu of variable declare using the const keyword can't be reaassign

// interest = 4.489;
// console.log(interest);


//Reasign variable vs. Initializing
//Declare variable

let supplier;
//initializing
supplier = "John Smith Tradings";
// Reassigning
supplier = "Uy's Trading";

//Declaring and intializing a variable
let consumer = "Chris";
//reassigning
consumer = "Topher";

// Can you declare const variable without 
	//No an an erro wil occur
// const driver;
// driver = "Warlong Jay";


// var vs let/const keyword
	// var is also use in declaring variables but var an ecmascript one version(1197)
//Ecmascript new ver let/const wast introduc as a new feature in ES6(2015)

//var keyword in js is hoisted 

//what makes let/const different from var?
//there are issues aassociated with variables declare or created using var regarding hoisting
//Hoisting is javascript default behavior of moving declaration to the top
// in terms of varibles and const
//keyword var is hoisted and let/const does not allow hoisting

//example of hooisted

	a = 5
	console.log(a)

	var a;

	// b=6;
	// console.log(b);
	// let b;

//let/const local/glabal scope
//scope means where this vaariable are avialble or accesible for use

//let and const are block scoped
// a block is a chunk of code bounded by{} a block lives in a curly braces. anything within the braces are block

let outerVariable = "hello";

let globalVariable;
{
	let innerVariable = 'hello again'
	console.log(innerVariable);

	globalVariable = innerVariable;
}
// console.log(letinnerValue);
console.log(globalVariable)
// Multiple variable declarations and intialization
// multiple variables maybe declared in one statement

let productCode ="DC017" ,productBrand = "Dell";
console.log(productCode);
console.log(productBrand);
//pagmultiple console one liner
console.log(productCode,productBrand);

//Using a variable with a reserve keyword
//reserved keywopord not allowed to use a variable name as it has function in javascript
// const let = "Hi Im let keyword";
// console.log(let);

// [Section] Data types

//Strings 
//Strings are a series of charcters that create a word, a phrase a sentence or anything related to craeteng text
//String in Javascript can be written using either a single('') or doubl ("") qoute

let country = 'Philippines';
let province = "Metro Manila";
     // Concatenation of strings in javascript
		//Multiple string values can be combined create a single string using + symbol

let fullAddress = province +','+ country;
console.log(fullAddress);

let greeting = 'I live in the '+ country;
console.log(greeting);
/*
Metro Manila
Philippines

The escape charcter (\) in string in combination
with other character can produce different efects/results
	"\n" this creates a next line in between text
*/
let mailAddress = 'Metro Manila\n\n Philippines';
console.log(mailAddress);

let message = "John's employees went home early.";
console.log(message);

// use of concatention or escape characters 
message = 'John\'s employees went home early.';
console.log(message);

// Numbers
//Integer/Whole Numbers

let headcount = 26;
console.log(headcount);

// Decimal Number/Fractions

let grade = 98.7;
console.log(grade);

//Exponential Notation

let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and numbers

console.log("John's grade last quarter is "+grade);

// Boolean base on certain things
// Boolean values are normally used to create values relating to the state of certain things.

let isMarried = false;
let isGoodConduct =true;

console.log("isMarried: "+isMarried);
console.log("isGoodConduct: "+isGoodConduct);

// Arrays 
// Arrays are special kind of data that's used to store multiple related values.
//Arrays can store different data types but is normally used to store similar data types

// similar data types 
//Syntax:
	//let/const arrayName = [elementA, elementB, elementC,...]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data types 
// not recommended in using array
let details =["John","Smith", 32, true];
console.log(details);

//Objects
//Objects are another spcial kind of data type that use to mimic real world object or items
//

/*
Syntax:
Let/const objectName = {
	propertyA: valueA,
	propertyB: valueB,
}

*/

// properties in obeject is descriptive
let person = {
	fullName: "Juan Dela Cruz" , 
	age: 35,
	isMarried: false,
	contatc: ["+63917 123 4567","8123 4567"],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
}

console.log(person);

// typeof operator is used to detrmine the type of data or value or variable. it outputs strings

console.log(typeof mailAddress);
console.log(typeof headcount);
console.log(typeof isMarried);

console.log(typeof grades); console
// note array is a special type of object with methods and funtion to manipulate eit 

// Constant obkject and arrays

// the keyword const is a little misleading , it does not define a cosntant value , it defines a cconstant reference to a value

// beacause of this you cannot
// Reassign a constant value
// Reassign a constant array
// Reassign a constant object

/* but you can 

changes the elemts of a constant array
change the properties of contant object */

const anime = ["One piece", "One punch man", "attack on titan"];

/*anime =["One piece", "One punch man", "kimetsu no yaiba"]; cant do this to change the value*/

// instead do this
anime[2] = "kimetsu no yaiba";
console.log(anime);

// Null
// its used to intentionaly express the absence of a value in a variable


let spouse = null;

spouse ="Maria"

// undefined represents the state of a variable that has been declred but twithout an assigned value;

let fullName;

fullName = "Maria";