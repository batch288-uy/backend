// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object

// Invoke the tackle method and target a different object

let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May","Max"],
        kanto: ["Brock","Misty"]      
    },
    talk: function(){
        return this.pokemon[0]+ "! I choose you!";
    }
}

function Pokemon(name, level){
    this.name = name,
    this.level = level,
    this.health = 10 * level;
    this.attack = 5 * level;
    
    this.tackle = function(target){
        target.health = (target.health - this.attack);
        console.log(this.name + " tackled " + target.name);
        console.log(target.name + "'s health is now reduced to " + target.health);
        if(target.health <= 0){
            return this.faint(target);
        }
    },
    this.specialAttack = function(target){
        this.attack = (this.attack * 10);
        target.health = (target.health - this.attack);
        console.log(this.name + " tackled " + target.name);
        console.log(target.name + "'s health is now reduced to " + target.health);
        if(target.health <= 0){
            return this.faint(target);
        }
    },
    this.faint = function(target){
        return target.name + " fainted.";
    }
}

let pikachu = new Pokemon("Pikachu", 20);
let geodude = new Pokemon("Geodude", 20);
let mewtwo = new Pokemon("Mewtwo", 100);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}
