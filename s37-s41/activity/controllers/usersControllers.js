const Users = require("../models/Users.js");
const Courses = require("../models/Courses.js");

const bcrypt =require("bcrypt");

//Require the auth.js
const auth = require('../auth.js');

// Controllers

//Create a controller for the signup
//registerUser
/* Business Logic/ Flow */
 	// 1. First, we have to validate whether the user is existing or not. We can do that by valiadting whether the email exist on our databases or not.
	//2. If the user email is existing, we will prompt an error telling the user that the email is taken.
	//3. otherwise, we will sign up or add the user in our database.
module.exports.registerUser = (request, response) => {
	
	//find method: it will return the an array of object that fits the given criteria

	Users.findOne({email : request.body.email})
	.then(result => {

		// we need add if statement to verify whether the email exist already in our database
		if(result){

			return response.send(false);
		}else{

			//Create a new Object instantiated using the Users Model.
			let newUser = new Users({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				//hashSync method : it hash/encrypt our password
				//the second argument salt rounds
				password: bcrypt.hashSync(request.body.password, 10),
				isAdmin: request.body.isAdmin,
				mobileNo: request.body.mobileNo
			})


			// Save the user
			//Error handling
			newUser.save()
			.then(saved => response.send(true))
			.catch(error => response.send(false));



		}

	})
	.catch(error => response.send(false));

}

//new controller for the authentication of the user
module.exports.loginUser = (request, response) => {

	Users.findOne({email : request.body.email})
	.then(result => {

		if(!result){
			return response.send(false)
		}else{
			//the compareSync method is used to compare a non encrypted password from the login form to the encrypted password retrieve form the find method. returns true or false depending the result of the comparison.
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				return response.send({ auth : auth.createAccessToken(result)}
				)
			}else{
				return response.send(false)
			}


		}

	})
	.catch(error => response.send(false));

}

module.exports.getProfile = (request, response ) => {

	const userData = auth.decode(request.headers.authorization);

	// console.log(userData);

	if(userData.isAdmin){

		Users.findById(request.body.id)
		.then(result => {

					// Changes the value of the user's password to an empty string when returned to the frontend
					// Not doing so will expose the user's password which will also not be needed in other parts of our application
					// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
					result.password = "Confidential";


					// Returns the user information with the password as an empty string
					return response.send(result);

				});

	}else{

		return response.send(`You are not an admin, you dont't have access to this route.`);

	}

	
}

//Controller for the enroll course.
module.exports.enrollCourse = (request, response) => {

	const courseId = request.body.id;
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		return response.send(false);
	}else{

		//Push papunta kay user document
	let isUserUpdated =  Users.findOne({_id : userData.id})
		.then(result => {

			result.enrollments.push({
				courseId : courseId
			})

			result.save()
			.then(saved => true)
			.catch(error => {
				console.log(error)
				return false})


		})
		.catch(error => {
				console.log(error)
				return false})



		//Push papunta kay course document

	let isCourseUpdated =  Courses.findOne({_id : courseId})
		.then( result => {

			result.enrollees.push({
				userId : userData.id

			});


			result.save()
			.then(saved => true)
			.catch(error => {
				console.log(error)
				return false})

		})
		.catch(error => {
				console.log(error)
				return false})

		//If condtion to check whter we updated the users doucment and courses document

		if( isUserUpdated && isCourseUpdated){
			return response.send(true);
		}else{
			return response.send(false);
		}


	}
}

module.exports.retrieveUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	Users.findOne({_id : userData.id})
	.then(data => response.send(data));
}