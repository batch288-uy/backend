const express = require('express');
const mongoose = require('mongoose');

//It will allow our backend application to be available to our frontend application
//It will also allows us to control the app's Cross Origin Resource Sharing settings.
const cors = require('cors');

const usersRoutes = require('./routes/usersRoutes.js');

const coursesRoutes = require('./routes/coursesRoutes.js');

const port = 4001;

const app = express();


//Mongod DB connection
// Establish the connection between the DB and the application or server.
//THe name of the databse should be :"CourseBookingAPI"
	mongoose.connect("mongodb+srv://admin:admin@batch288uy.vm2f3w2.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	})

	let db = mongoose.connection;

	db.on("error", console.error.bind(console, "Network problem, can't connect to the db!"))

	db.once("open", ()=>console.log('Connected to the cloud database!'))
//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Reminder that we are going to use this for the sake of the bootcamp
app.use(cors());

//add the routing of the routes from the usersRoutes
app.use('/users', usersRoutes);
app.use('/courses', coursesRoutes);

if(require.main === module){
	app.listen(process.env.PORT || 4000,()=>{
		console.log(`AP is now online on port ${process.env.PORT || 4000}`)
	});
}


