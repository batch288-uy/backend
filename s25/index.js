// console.log("Good game Lakers!");

// [SECTION] JSON Objects
	//JSON stands for JavaScript Object Notation
	//JSON is also used in other programming languages hence the name JavaScript Object Notation.
	//Core JavaScript has a built in JSON object that containes methods for parsing objects and converting strings into JavaScript objects.
	//.json file, JSON file
	//JavaScript objects are not to be confused with JSON
	//JSON is used for serializing different data types into bytes
	//Serialization is the process of converting data into series of bytes for easier transmission/ tranfer of information/data.
	//A byte is a ubit of data that is eigth binary digits( 1 , 0) that is used to represent a character (letter, numbers, typographic symbols.)
	/*
		Syntax: {
			"propertyA" : "valueA",
			"propertyB" : "valueB"
		}
	*/

	// [Section] JSON Arrays

/*
"cities" : [
		{"city: "Quezon City, "province" : "Metro manila",
		"country : "Philippines"}
]

*/
//[Section] JSON Methods
//The JSON object contains method for parsing and converting data into stringified JSON

//Converting Data into stringified JSON , Stringified json is a javascript object converted into a string to be used in other functions of a javascript application
//They are commonly used in http request where information is required to be sent and receive in a stringified version

//Requests are  important part of programming where an application communicates with a backend application to perform different tasks suchs as getting/creating data in database

let batchesArr = [{batchName: 'Batch X'},{batchName : 'Batch Y'}]

console.log(batchesArr);
console.log(typeof batchesArr); //always remeber si array object tlaga yan
//the stringify method is ised to convert javasacript
//objects into a string

console.log('Result from sytingify method: ');
//if you are going to convert objects to string , you'll use json.stringify(variableName)
console.log(JSON.stringify(batchesArr));
console.log(typeof JSON.stringify(batchesArr));

/*
Using stringify method with variables

	Syntax:
		JSON.stringify({
			"propertyA" : "valueA",
			"propertyB" : "valueB"
		})

*/
//user details
/*let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
	city: prompt('Which city do you live in?'),
	country : prompt('Which country does your city address belongs to')
};

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age ,
	address : address
});

console.log(otherData);
*/

//converting stringified into javascript objects 
//objects are common data types used in applications because of the complex data structures that can be created out of them

let batchesJSON = '[{"batchName" : "Batch X"},{"batchName" : "Batch Y"}]';

console.log(batchesJSON);

console.log('Rresult from parse method: ');
console.log(JSON.parse(batchesJSON));

let exampleObject = '{"name" : "Crhis" , "age": 16 , "isTeenager": false}';
console.log(JSON.parse(exampleObject));

let exampleParse = JSON.parse(exampleObject);

console.log(typeof exampleParse.name);
console.log(typeof exampleParse.age);
console.log(typeof exampleParse.isTeenager);
