const express = require("express");
const mongoose = require("mongoose");

const taskRoutes = require('./routers/taskRoutes.js');


const port = 4000;


const app = express();


// Set up MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch288uy.vm2f3w2.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser: true});


// Check whether we are connected with our db
const db = mongoose.connection;
	
	db.on("error", console.error.bind(console, "Error, can't connect to the db!"));

	db.once("open", () => console.log('We are now connected to the db!'));


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//this route/middleware is responsible for the routes in tasks
app.use("/tasks", taskRoutes);




app.listen(port, () => console.log(`The server is running at port ${port}!`));