// console.log("Hello Batch 288!");

//[Section] javascript synchronous vs asynchronous
// by default javascript is synchronous meaning that only one statement is executed a time

//this can be proven when a statement has an error , javascript will  not proceed with the next statement // conole.log("Hello after the world");

// console.log("Hello world");

// conole.log("Hello after the world");

// console.log("Hello")

//when certain statements take a lot of time to process , this slows down our code

// for(let index = 0 ; index <= 100000; index++){
// 	console.log(index);
// }
// console.log("Hello again");

//asynchronous means that we can proceed to execute other statements, while time xonsuming code is running in the background

//[Section] Getting all posts
//the fetch API allows you to asynchronously request a resource data
// so it means the fetch method that we are going to use here will run asynchronously 
// Syntax:
	//fetch('URL')
	
	//A promise' is an object that represents the eventuzal completion or faillure of an asynchronous function and its resulting value
	console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

	//syntax 
	/*
		fetch('url')
		.then((response => response));
	*/

		//this will retrive all POST follwo the REST API

	// the fetxh method will return a promise that resolves the response object
	// the then' method captures the response object 
	fetch('https://jsonplaceholder.typicode.com/posts')
	// .then(response => console.log(response));


	// use json method from the response object to convert the data retrieved into JSON format to be used in our application
	//pagmutiple line or curly braces need gumamit ng return
	.then(response => response.json())
	.then(json => console.log(json))

	//The 'async and await' keyword , it is another approach that can be used to achieve asynchronous code' ginagamit lng sa mga function

	//Creates an asynchronous function

	//this is a normal function 
	function fetchData(){

	}

	//remeber async and await partner
	//this function is asynchronous
	async function fetchData(){

		let result = await fetch('https://jsonplaceholder.typicode.com/posts')

		console.log(result)

		let json = await result.json();

		console.log(json);

	}

	fetchData();

//fetch data();

//[Section] Getting specific POST

	//retreieves specific post follwong the rest  API(/posts/:id)

	fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json())
	.then(json => console.log(json));

	//[section] Creating post
	/*
		syntax
		
			options  ' is an object that contains the method , the header and the body of the request'

			by default if you dont add the method in the fetch , it will be a GET method
		fetch('URL', options)
		.then(response => {})
		.then(response => {})

	*/
fetch('https://jsonplaceholder.typicode.com/posts', {
	//it sets the method of the request object to Post following the rest API
	method: 'POST',
	//sets the header data of the request object to be sent to the backend
	//specified that the content will be in JSON structure
	headers: {
		'Content-type' : 'application/json'
	},
	//sets the content/body data of the request object to be sent to backend
	body: JSON.stringify({
		title : 'New post',
		body: 'Hello World',
		userID: 1

	})
})
.then(response => response.json())
.then(json => console.log(json))

//[Section] update an specific post

fetch('https://jsonplaceholder.typicode.com/posts/1',{

	method: "PUT",
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({

		title:'Update Post'

	})
})
.then(response => response.json())
.then( json => console.log(json))

//PATCH
fetch('https://jsonplaceholder.typicode.com/posts/1',{

	method: "PATCH",
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({

		title:'Update Post'
	
	})
})
.then(response => response.json())
.then( json => console.log(json))

//the put method is a method of modifying resource where the client send data that updates the entire object/document

//Patch method applies a partial update to the resources or to the object or document

// [Section] Deleting a POST 
//deleting specific post follwing the REST API
fetch('https://jsonplaceholder.typicode.com/posts/1', {method : "DELETE"})
.then(response => response.json())
.then(json => console.log(json));