let http = require("http");

const port = 4000;

const app = http.createServer(function(request, response) {
    if (request.url === "/" && request.method === "GET") {

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Welcome to the Booking System');

    } else if (request.url === "/profile" && request.method === "GET") {

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Welcome to your profile');

    } else if (request.url === '/courses' && request.method === "GET") {

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end("Here's our available courses");

    } else if (request.url === '/addCourses' && request.method === "POST") {

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end("Add courses to our resources");

    } else if (request.url === '/updateCourses' && request.method === "PUT") {

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end("Update courses to our resources");

    } else if (request.url === '/archiveCourses' && request.method === "DELETE") {

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end("Archive courses to our resources");

    } else {
        response.writeHead(404, {'Content-Type': 'text/plain'});
        response.end("404 Not Found");
    }
 })
 app.listen(port)

console.log(`Server is now accesible at localhost:${port}.`)




Do not modify
Make sure to save the server in variable called app
 if(require.main === module){
     app.listen(4000, () => console.log(`Server running at port 4000`));
 }

 module.exports = app;
