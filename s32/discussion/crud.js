let http = require("http");

//mock database
let directory = [
{
	"name" : "Brandon",
	"email": "brandon@gmail.com"
},
{
	"name" : "Jobert",
	"email": "jobert@gmail.com"
}

	]

http.createServer(function(request, response) {

	//Get method
	if(request.url == "/users" && request.method == "GET"){
		//'application/json' - sets response output to Json  data type
		response.writeHead(200, {'Content-Type': 'application/json'});

		//response.write() method from node.js that is used to wite to the response body in a http server
		//JSON.stringify() - method vonverts the string to input json
		response.write(JSON.stringify(directory));
		response.end();
	};

	//post method

	if(request.url == "/addUser" && request.method == "POST"){

		//declare and initialize of requestBody variable to an empty string

		let requestBody = "";

		//event listener sa nod js and request.on
		//use to handle incoming data in HTTP server

		//data is received from the client and is processed in the data ' stream'

		request.on('data',function(data){
			requestBody += data;
		});

		request.on('end',function(){

			requestBody = JSON.parse(requestBody);

			let newUser = {
				"name" : requestBody.name,
				"email" : requestBody.email
			}

			//add the new user into the mock database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type':'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		}); 
	}

}).listen(3000)

console.log('Server running at localhost: 3000');
