//use the 'require' directive to load the express module package
//it will allow us to access methods and functions that will make us create easily our application or server
// a ' module ' is a software component for part of a programa contains one or more routines
const express =require("express");

//create an application using express
//creates an express application and stores this in an constant variable called app
const app = express();

//for our application server to run we need a port to listen 
const port = 4000;

//midllewares
//act as mid man , it is a software the provides common services and capabilities to application outside of what offered by the operating system

//allow our application to read json data
app.use(express.json());

//this one will allow us to read data from  forms
//by default , information received from the url can only be received a string or an array

//by applying the option of extended : true, this will a,llow us to recv information in others data types such as an objectw which will use throughout our application
app.use(express.urlencoded({extended: true}))


//[section] Routes
//express has methids corresponding to each http method
//this route expects to receive a GET request at the base uri "/"

app.get("/", (request,response) => {
	//once the route is accesed it will send a string response containing "hello world"

	//compred to prev session , .end uses the node js modules's method

	//.send method uses the expressJs modules method
	//instead to send a response back top the client

	  response.send('Hello Batch 288!')
})
app.get("/hello" , (request, response) => {
	response.send("Hellow from the/hello endpoint")
})

//this route expects to recv a POST requwst at the URI "/hello"
app.post("/hello" , (request, response) => {
	//request.body contains the  content/data of the request body
	//all the properties defined in our POSTMAN request will be accessible here as properties with the same name
	console.log(request.body);

	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`)
})
//this array will store user objects when the "signup route is accessd"
//this will serve as out mock database 
// let users = [];


// app.post("/signup", (request,response) =>{
// 	console.log(request.body);

// 	if(request.body.username !== "" && request.body.password !== "")
// 	{
// 		users.push(request.body)
// 		response.send(`User ${request.body.username} sucessfuly registered!`)
// }
// else{
// 	response.send('Please input BOTH username and password')
// }

// 	// response.send("Trial");

// })

//this route expects to revd a PUT request at the URI "/change-password"
//this will upted the password of a user that matches the info provided in the client.postman

// app.put("/change-password", (request,response) =>{
// 	let message;

// 			for(let index = 0 ; index < users.length; index++){
				
// 				//If the provided username in the client/postman and the username of the current object in the loop is the same
// 				if(request.body.username == users[index].username){
						
// 					users[index].password = request.body.password

// 					message = `User ${request.body.username}'s password has been updated!`
// 					break;
// 				}else{
// 					message = 'User does not exist!'
// 				}

// 			}
// 			response.send(message);
// 	})


//[Section]Activity Part 

///home
app.get("/home" , (request, response) => {
	response.send("Welcome to the Home page")
})

//users
let users = [
  {
    username: "johndoe",
    password: "johndoe1234"
  },
  {
    username: "asus",
    password: "asusnambawan"
  },
  {
    username: "mikmik",
    password: "mikmikmatamis"
  },
  {
    username: "Kokushibo",
    password: "sixeyes"
  }
];

app.get("/users", (request, response) => {
  response.json(users);
});

// delete-user
app.delete('/delete-user', (request, response) => {
  let message = "";
  let userIndex = -1;

  if (users.length > 0) {

    for (let index = 0; index < users.length; index++) {

      if (request.body.username == users[index].username) {
        userIndex = index;

        break;

      }
    }

    if (userIndex !== -1) {
      let user = users[userIndex].username;
      users.splice(userIndex, 1);

      message = `User ${user} has been deleted`;
    } 
     else if (message === undefined) { 
      message = "User does not exist";
  }
   else {
    message = "No users found";
  }
}
  response.send(message);
});


//tells our server to listen to the port
//if the port is accese we can run the server 
//return a message cinfirming that the server is running in the tterminal

if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;

