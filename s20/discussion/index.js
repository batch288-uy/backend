// console.log("Hello World!");

//[Section]while loop
	// a while loop takes an expression/condition
//Expressions are any unit of code that cna be evaluated as true or false. If the condition evaluates to be a true , the statements/code block will be executed
// A loop wi;; iterate a certain number of times until an expression/condition is true
//Iteration is the term given to repition of satetmensts

/*
Syntac:
while(expression/condiotn){
	statement;
	increment/decrement;
}
*/
let count =5;
// while the value of count is not equal to zero the statement inside will run/iterate
while(count !== 0){

	//the current value of count is printed out 
	console.log("While:" + count)

	//Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
	//forgetting to include this in our loops will make our application run an infinite loop which will eventually crash our device
	count--;
}

//do while gurantees mageexecute atleast once


// let number = Number(prompt("Give me a number:"));
// //The number function works similarly to the "parseInt" function.


// do{
// 	console.log("Do While: " + number);
// 	// increment
// 	number++;
// }
// while (number < 10);

//for loop is more flexible
/*
a for loop is more flexible than while and do while loops it consist 3 parts
1.The initialization ' value that will track the pregression of the loop'
2.expression/condition that will evaluated which will determine whether the loop will run one more time
3.The iteration which indicates how to advance the loop

Syntax:
for(initialization;expression/condiotn oteration){
	statements;
}
*/
for(let count = 0; count <=20; count++)
console.log("The current value of count is " + count);

let myString = "alexis";
//.lentght porperty
//characters in strings maybe counted usign the .length property

console.log(myString.length);

console.log(myString[0]);
console.log(myString[3])

console.log(myString[5]);

for (let index = 0; index < myString.length; index++) {
	console.log(myString[index])
}

//Create a string named "myName " with value of Alex;

let myName = "Alex";
/*
create a loop that will print out the letters of the name individually and prinout the number 3 instead when the letter be printed is a vowel
*/

// for(let index =0;index < myName.length; index++)
// {
// 	// console.log(myName[index]);
// 	 if (letter === 'a' || letter === 'e' || letter === 'i' || letter === 'o' || letter === 'u') {
// 	    console.log(3);
// 	  } else {
// 	    console.log(myName[index]); 
// 	  }
// 	}


// Let myNickName = "Chris";

// let updatedNickNAme myNickName.replace("Cris","Chris")

//[Section]Continue and break statements
	//the "continue" statements allows the code to go to the next iteration of the loop wihtout finishing the execution of all sattements in code block
	//the "break" statements is used to terminate the current loop once a match has been found

	//Create a loop that if the count value is divided by and the remain is 0, it will print the number and continue to the next iteration of the loop.
console.log("------------------------------------------")
	for(let count = 0; count <=20; count ++){

		if(count >=10){
			console.log("The number is equal to 10, stopping the loop");
			break;
		}

		if(count % 2 === 0){
		console.log("the number is divisible to two,skipping....s");
		 continue;
		}
		console.log(count);
}
// Create a loop that will iterate based on the legnth of the string name


console.log("------------------------------------------")
let name = "alexandro";
//we are going to console the letters per line, and if the letter is equal to a "a" we are going to console "Continue to the next statement"
//if the current letter is equal to "d" we are going to stop the loop

for (let index = 0; index < name.length; index++) {
	if(name[index]==="a"){
		console.log("Continue to the next iteration")
		continue;
	}
	else if(name[index == "d"]){
		break;
	}
	else{
		console.log(name[index]);
	}

	}


