//show database - list of db inside our cluster
//use dbName - to use specific database
//show collections - see the list of collections inside the db

//CRUD operation
/*
- crud operation is the heart of any backend application
-mastering the CRUD operation is essential for any developer especially to those who want to become
backend developer

*/

//[Section] insert Document (Create)
// Insert one document
 /*
	since mongoDB deals with objects as its structure for documents we can easily create them by providing objects in or method/operation

Synatx
	db.collectionName.insertOne({
	object 
	})

 */

db.users.insertOne({
	firstName : "Jane",
	lastName:  "Doe",
	age : 21,
	contact : {
		phone : "123456789",
		email : "janedoe@gmail.com"
	},
	courses : ["CSS", "Javascript", "python"],
	department : "none"
})

//Insert Many

/*
Syntax :
 db.collectionName.insertMany([{objectA},{objectB}])

*/

db.users.insertMany([
{
	firstName : "Stephen",
	lastName : "Hawking",
	age: 76,
	contact : {
		phone : "87654321",
		email : "stephenhwaking@gmail.com"
	},
	courses: ["Python","React","PHP"],
	department : "none"
},

{
	firstName : "Neil",
	lastName : "Armstrong",
	age: 82,
	contact : {
		phone : "87654321",
		email : "neilarmstrong@gmail.com"
	},
	courses: ["React","Laravel","Sass"],
	department : "none"

  }
])

db.userss.insertOne({
	firstName : "Jane",
	lastName:  "Doe",
	age : 21,
	contact : {
		phone : "123456789",
		email : "janedoe@gmail.com"
	},
	courses : ["CSS", "Javascript", "python"],
	department : "none"
})

//[Section] finding documents (Read operations)
/*
  db.collectionName.find();
  db.collectionName.find({field:value});
*/
//using the find method it will show you the list of the documents inside collections
db.users.find();

db.users.find().pretty(); // pretty method allows us to be able to view the documents returned by or terminals to be in a better format


db.users.find({firstName: "Stephen"}); //it will return the documents that will pass the criteria given in the method

db.users.find({"_id" :ObjectId( "646c57991acbc40e451686b0")})

//multiple criteria 

db.users.find({lastName : "Armstrong", age : 82});

db.users.find({ firstName : "Jane"});

//[Section ] Updating documents(Update)

db.userss.insertOne({
	firstName : "test",
	lastName:  "test",
	age : 21,
	contact : {
		phone : "00000000",
		email : "test@gmail.com"
	},
	courses : ["CSS", "Javascript", "python"],
	department : "none"
});

db.users.updateOne(
{
	firstName: "test"
},
{
	$set: {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Lavarel", "HTML"],
		department: "Operations",
		status: "none"
	}
});

db.users.updateOne(
	{ firstName : "Jane"},
	{
		$set: {
			lastName : "edited"
		}
	});

// updating multiple documents
/*
syntax 
db.collectionName.updateMany(
{criteria},
{
	$set: {
	{field:value}
	}
})
*/

db.users.updateMany(
	{department : "none"},
	{
		$set: {
			department : "HR"
		}
	});

//replace one
/*
  syntax: db.collectionName.replaceOne(
  {criteria}, {
	$set : {
	object
	}
  })

*/

db.users.insertOne({firstName: " test"});

db.users.replaceOne({
	firstName: "test"},
	{
		
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {},
			courses: {},
			department: "Operations"
		
	});

//[Section] Deleting documents

//deleting single document
/*
db.collectionname.deleteOne({criteria})
*/

db.users.deleteOne({firstName : "Bill"});

//deleting multiple document
/*

*/

db.users.deleteMany({ firstName : "Jane"});