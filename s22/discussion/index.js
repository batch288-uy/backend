// console.log("Hello World")

//Array Methods
//javascirpt has built - in fucntions and methods for arrays. This allows us to manipulate and access array items.

//Mutator methods
//Mutator methods are functions that mutate or change an array after they're ccreated
//These methods manipulate the original array performaning various task such as adding or removing elements.

let fruits = ['Apple','Orange','kiwi','Dragon Fruit'];

//push()

/*
 add an element in the end of an array and returns the updated array's length
 Syntax:
 arrayName.push();
*/
console.log("Crrent array: ");
console.log(fruits);

let fruitsLength = fruits.push('Mango');

console.log(fruitsLength);
console.log("Mutated array from push method: ");
console.log(fruits);

//pushing multiple elements to an array

fruitsLength =fruits.push('Avocado','Guava');

console.log(fruitsLength);
console.log("Mutataed array after pushing multiple elements: ")
console.log(fruits);

//pop()
/*
 removes the last element and returns the removed element
 Syntax 
 arrayName.pop()
*/

console.log("Current Array: ");
console.log(fruits);

let removedFruits = fruits.pop();
console.log(removedFruits);
console.log("Mutated Array from the pop method: ");
console.log(fruits);

//unshift()
/*
it adds one or more elements at the beginning of an array
Syntax
arrayName.unshift(elemenA')

if multiple//
rrayName.unshift(elemenA',elementB' ....)
*/

console.log("Current Array: ");
console.log(fruits);

fruitsLength = fruits.unshift('Lime','Banana');

console.log(fruitsLength);
console.log("Mutated array from unsshift method: ");
console.log(fruits);

//shift()
/*
removes an element at the beginning of an array and returns the removed element
Syntax
arryname.shift()
*/

console.log("Crrent Array: ");
console.log(fruits);

removedFruits = fruits.shift();

console.log(removedFruits);
console.log("Mutated array from the shift method");
console.log(fruits);

//splice()
/*
simultaneously removes an elements from a specified index number and add elemnt
syntax
arrayname.splice(startingIndex,deleteCount,elementsToBeAdded)
*/

console.log("Crrent Array: ");
console.log(fruits);


//paggusto ko magdelete sa pinakadulo
//fruits.splice(fruits.lenghth -1,1);
//paggusto ko magaad sa dulo ng cherry
fruits.splice(fruits.lenghth,0,"cherry");
fruits.splice(4,2, "Lime","Cherry");
console.log("Mutated array after the splice method: ");
console.log(fruits);

//sort() method
/*'
rearranges the array elements in alphanumeric order
syntax:
arrayName.sort();
*/

console.log("Crrent Array: ");
console.log(fruits);

fruits.sort();
console.log("Mutated array from sort method: ");
console.log(fruits);

//reverse() method
/*
revrse the order of array elemnts
syntax 
arrayName.reverse()
*/

console.log("Crrent Array: ");
console.log(fruits);

fruits.reverse();
console.log("Mutated array from reverse method: ");
console.log(fruits);

// [Section] Non-mutator methods
/*
non mutator methods are functions taht do not modify or change an array after the're created 
//these methods do not manipulate the orignal array performaning various task such as returning elements from an array and combining arrays and prinitng the output

*/

let countries = ['US','PH','CAN','SG','TH','PH','FR','DE'];

//indexOf()
//it returns the index number of the first matching elemnt found in array
//if no match was found the result will be n-1
//the seach process will be done from the first element proceeding to the last elemnt
/*
synatx 
arrayName.indexOf(searchValue)
arrayName.indexOf(searchValue,startingIndex)
*/

let firstIndex =countries.indexOf('PH');
console.log(firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log(invalidCountry);


firstIndex = countries.indexOf('PH',2);
console.log(firstIndex);


console.log(countries);


// lastIndexOf()
/*
	-returns the index number of the last matching element found in an array
	- the search process will be done from last element proceeding to the first element
		syntax:
			arrayName.lastIndexOf(searchValue);
			arrayName.lasIndexOf(searchValue, startingIndex);
*/

let lastIndex = countries.lastIndexOf('PH');
console.log(lastIndex);

invalidCountry = countries.lastIndexOf('BR');
console.log(invalidCountry);

lastIndexOf = countries.lastIndexOf('PH',  	6);
console.log(lastIndexOf);

// indexOf, starting from the starting index going to the last element(from left to right);
//lastIndexOf, starting from the starting index going to the first element(from right to left);

console.log(countries);

// slice()
	/*
		-portions/slices elements from an array AND retrun a new array
		-syntax:
			arrayName.slice(startingIndex);
			arrayName.slice(startingIndex, endingIndex);
	
	*/

// Slicing off elements from a specifeid index to the last element.

let slicedArrayA = countries.slice(2);
console.log('Result from slice method:');
console.log(slicedArrayA);

// Slicing off elements from a specified index to another index:
// The elements that will be sliced are elements from the starting index until the element before the ending index.
let slicedArrayB = countries.slice(2, 7);
console.log('Result from the slice method:');
console.log(slicedArrayB);

//slicing off elements starting from the last element of an array:
let slicedArrayC = countries.slice(-5);
console.log('Result from the slice method:');
console.log(slicedArrayC);


//toString()
/*
returns an array as string seperated by commas
syntax:
arrayName.toString();
*/

let stringArray = countries.toString();
console.log("Result from to string method: ")
console.log(stringArray);
console.log(typeof stringArray);

//concat()
//combines arrays to an arry or elments and returns the combined result
/*
Syntax
arrayA.concat(arrayB);
//arrayA.concat(elementA);
*/

let taskArrayA = ["drink HTML","eat javascript"];
let taskArrayB = ["inhale CSSL","breathe sass"];
let taskArrayC = ["get git","be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log(tasks);

//array to an elemnt 

let combinedTasks = taskArrayA.concat('smell express','throw react');
console.log(combinedTasks);

//concat multiple array into an array

let allTasks =taskArrayA.concat(taskArrayB,taskArrayC);
console.log(allTasks);

//join()
//returns an array as string seperated by specified seperator string

let users = ['John','Jane','Joe','Robert'];

console.log(users.join());
console.log(users.join(''));
console.log(users.join(" - "));

//[Section] Iterator methods
//iteration methods are loops designed to perform repititive task
//iteration methods oloops over all items in an array

//forEach()
//similar to a for loop that iterates on each of array element
//syntax:
//arrayName.forEach(function(indivElement){statement};)

console.log(allTasks);

allTasks.forEach(function(task){
	console.log(task);
});

//filteredTask variable will hold all the elements from the allTasks array that has more than 10 characters
let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task)
	}
});
console.log(filteredTasks);

//map()
//creates new array 
// iterates on each element and returns new array with different values depending on the result of the funciton operation

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){

	return number * number;
})
console.log(numbers);
console.log(numberMap);

//every()
/*
it will check if all elements in an array meet the given condition
//return true values if all elements meet the condition and false otherwise

syntax:
 let/const resultArray - arrayName.every(
 function(indivElement){
	return expression/condiotion;
 })

*/

numbers = [1,2,3,4,5];

let allVallid = numbers.every(function(number){
	return(number < 6 )
})
console.log(allVallid);

//some()
//checks if atleast one element in the array meets the given condiotin
/*
syntax:
let/const resultArray = arrayName.some(function(indivElement)){
	return expression/condion
}
*/

let someValid = numbers.some(function(number){
	return (number < 2);
})

console.log(someValid);

//filter()
//returns new array that contains elements which meets the given 
//returns an empty array if no elements were found
/*
Syntax:
let/const resultArray = arrayName.filter(
function(indivElemnt){
	return expression/condtion
})
*/

numbers = [1,2,3,4,5];

let filterValid = numbers.filter(function(number){
	return(number % 2 === 0 );
})

console.log(filterValid);

//includes()
//checks if the argument passed can be found in the array

let products = ['Mouse','Keyboard','Laptop','Monitor'];

let productFound1 = products.includes('Mouse');

console.log(productFound1); /*case sensitive*/

//reduce()
//evaluates element from left to right and returns/reduces the array into single value
//the first parameter in the function will be the accumulator (x) and the second parameter will be the current value(Y)

numbers = [1,2,3,4,5];
let reducedArray = numbers.reduce(function(x,y){
	console.log('Accumulator  ' + x);
	console.log('CurrentValue  ' + y);
	return x+ y;
})
console.log(reducedArray);

