// console.log("Hello Batch 288");

// [Sectiom] Arithmetic Operators
 let x = 1397;
 let y =7831;

 //Addition Operator(+)

 let sum = x+ y ;

 console.log("Result of Addition opperator: " + sum);

 //subtraction Operator

 let difference = y -x ;
 console.log("Result of subtraction operator: " + difference);

 // multiplication Operator

 let product = x * y;
 console.log("Result of Multiplication operator: " + product);


 //division Operator (/)
 let quotient = y / x ;
 console.log("Result of division operator: " + quotient);
 // /.toFixed() methods

 // Modulo Operatot (%)

 let remainder = y%x;
 console.log("Result of modulo operator" + remainder);

 //[Section] Assignment operators
// Assignment operator (=) The assignment operator assign/reassign the value of the right operand to a variable and assign the value to the variable

let assignmentNumber = 8;

//Addition aSsignment Operator (+=)
//the addiotion assignment operator adds the value of the right operand to a variable and assigns the result to the variable

assignmentNumber += 2;
console.log(assignmentNumber);

assignmentNumber += 3;
console.log("Result of assignment Operator: " +assignmentNumber);

//Subtraction/Multiplication/Division assignment operator
//opeartor (-=, *= ,/=)

//Subtraction assigment operator
assignmentNumber -=2;
console.log("Result of substraction assignment operator: " + assignmentNumber);

//multiplication assignment operator
assignmentNumber *= 3;
console.log("Result of multiplication assignment operator: " + assignmentNumber);

//Division Assignment operator
assignmentNumber /= 11;
console.log("Result of division assignment operator:" + assignmentNumber);

// Multiple operator and Parenthesis (MDAS/PMDAS)
//mdas rule Multiplication or Diviosn firsth then addition and subtraction, from left to right

let mdas = 1 + 2 - 3 * 4 / 5;
/*
 1. 3*4 = 12
 2. 12/5 = 2.4
 3. 1 + 2 = 3
 4 . 3 -2.4 = 0.6

*/
console.log(mdas.toFixed(1));

let pemdas = 1 + (2-3) * (4/5);
	/*
	1. 4/5 = 0.8
	2. 2-3 =-1
	3. 1 + (-1) * (0.8)

	Mdas
	4. -1 * 0.8 = -0.8
	5. 1- 0.8
	6. 0.2
	*/
console.log(pemdas.toFixed(1));

//[Section] Increment and decrement
// operators that add or substract values by 1 and reassigning the value of the variable where the increment and decremnt appliead to

let z =1;
let increment= ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result od pre-increment: " + z);

increment = z++;
console.log("The result of post increment: " + increment);

console.log("The result of post increment: " + z);

x = 1;
let decrement = --x;
console.log("Result of pre-dcrement: " + decrement);
console.log("Result of pre decrement: " + x);

//post-drecremnt

decrement = x --;
console.log("Result of post-dcrement: " + decrement)
console.log("Result of post decrement: " + x);


//[Section] Type Coercion
/*
  type coercion is the automatic implicit conversion of values from one data type to another -this happens when operations are peformed on different data types taht would normally possible and yield irregular results

*/

let numA = '10';
let numB = 12;

let coercion = numA + numB;
// if you will going to add string and number it will concatenate its value
console.log(coercion); /* out put would be 1012*/
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);


// The boolean "true" is associated with the value of 1
let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);


//[Section] Comparison Operators

let juan = 'juan';

// Equality Operator (==)
/*
	it simply checks wthere the operand are equal/have the same content/value
	-returns a blooean value true\\
	Attempts to convert and compare operands different data types
	-differnt false
*/
console.log(1==1); //true
console.log(1==2) //false
console.log(1=='1'); //true
console.log(0 == false); //false
//compare two strings that are the same
console.log('JUAN' == 'juan'); //false // case sensitive
console.log(juan == 'juan');//true 


// Inequality operator (!=)
/*
 it also checks the operands are not equal or hae different content/values
 it returns boolean value
 Attempts to convert and compare operands different data types/coercion
*/

console.log(1 !=1); //false
console.log(1 !=2); //ture
console.log(1 != '1') //false
console.log(0 !=false) //false
console.log('JUAN' != 'juan'); //true
console.log(juan !='juan');// false


// Strict Equality operator (===)
//what if we really want to compare 2 values need to be more strict
/*
 it checks whethere the operand are equal/have the same content/value
 Also compares the data type of two values
*/

console.log(1===1); //true
console.log(1===2); //false
console.log(1 === '1');// false
console.log(0===false); //false
console.log ('JUAN'=== 'juan');//false
console.log (juan=== 'juan');//true


// strictly inequality operator (!==)

/*
 check whether the operands are not equal / have diffrent content/value
 also compare data types of 2 values
*/

console.log(1 !== 1);//false
console.log(1 !==2);//true
console.log( 1 !== '1'); //true
console.log(0 !==false); //true
console.log ('JUAN'=== 'juan');//true
console.log (juan=== 'juan');//false

//[SECTION] Relational Operators
//Chaeck whether one value is greater or lessthan other values

let a = 50 ;
let b = 65;

// GT or greater tahn operator (>)
let isGreaterThan = a > b; //false

//LT less than operator (<)
let isLessThan = a < b ; //true

//GTE greater than or equal opperator 

let isGTorEqual = a >= b; //false
//LTE less than or isGTorEqual (<=)

let isLTorEqual = a <= b; //true

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);


let numStr = "30";
console.log(a > numStr);
console.log(b <= numStr);

let strNum = "twenty";//20
console.log(b >= strNum);//false
//since the string is numeric, the string was converted to a number it resulted to NaN (Not a Number)

//[SECTION] Logical Operators
let isLegalAge = true ;
let isRegistered = false;

// Logical AND operator (&& double ampersand)
// Returns true if all operands are true
// isRegistered =true;
let allRequirementsMet = isLegalAge && isRegistered;//false
console.log("Results of logical AND operator:" +allRequirementsMet)

//logocal OR operator (|| double pipe)
//Return true if one of the operands are true


let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logocal OR operator " + someRequirementsMet);

//logical NOT operator (! exclamtion point)
//returns the opposite value

let someRequirementsNotMet = !isRegistered ; //true
console.log("Result of logical NOT operator: " + someRequirementsMet);
