// console.log("Hello World");

function printInput(){
	let nickname = "Chris";

	console.log("Hi, " + nickname);
}

printInput();
// for other cases , function can also proccess data directly passed into it instead of relying only on Global variables

//Consider this function:
function printName(name){
console.log("My name is " + name)
}
printName("Juana");

printName("Chris");

//variables can also be passed as an argument

let sampleVrariable = "Curry";

let sampleArray = ["Davis","Green","Jokic","Tatum"];

printName(sampleVrariable);
printName(sampleArray);

// one example of using the parameter and argument 
// function will check the number divisble by 8 or not 
//functionName(number)

function checkDivisibilityBy8(num){
	let remainder = num % 8;

	console.log("The remainder of " + num + " devided by 8 is " + remainder);
	let isDivisibleBy8 = remainder === 0;

	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}
checkDivisibilityBy8(68);

// functions as Argument
//function parameters can aslo accpet other functions as arguments

function argumentFunction(){
	console.log("This function was passed as an arguments before the message was printed")
}
	function invokeFunction(func){
		//pag di naglagay ng parethesis maglalagay ng parenthesis kay invoke
		func();
}
		invokeFunction(argumentFunction);

		function returnFunction(){
			let name = "Chris";
			return name;

		}

		function invokedFunction(func){
			console.log(func());
		}
		invokedFunction(returnFunction);
	
	//using multiple parameters

		//multiple "arguments will correspond to the number of parameters' declared in a function in suceeding order"

		function createFullName(firstName,middleName,lastName){
			console.log(firstName + " " +middleName + " "+ lastName);
		}

		createFullName('Juan','Dela','Cruz');

		// in javascript, providing more/less arguments than the expected will not return an error

		//providing less argument than the expected parameters will automatically assign an undefine value to the parameter
	createFullName('Gemar','Cruz')	

//using variable as multiple arguments

	let firstName = "John";
	let middleName ="Doe";
	let lastName = "Smith";

	createFullName(firstName,middleName,lastName);


// Using alert()
// alert allows us to show a small window at the top of our browser page to show information to our users.
//As opposed to consonsole log whihch only shows the message on the console

	// alert("Hellow World"); /* This well run immdeiately when page loads*/
	//Syntax: alert("MessageinString");

	//Alert inside a function

	function showSampleAlert(){
		alert("Hello User");
	}
	//important need to invoke
	// showSampleAlert();

	console.log("I will only log in the console when the alert is dismmised.");

	//using prompt
//promt allow us to show a small window at the top of the browser to gather user input
	//syntax prompt("Dialog in string")
	let smaplePrompt = prompt("Enter your name.");
	// console.log(typeof smaplePrompt)
	// console.log("Hello " + smaplePrompt);

	let age = prompt("Enter your age.");
	console.log(age);
	console.log(typeof age);
//returns an empty string when there is no input or null if the user cancels the prompt
	let sampleNullPrompt = prompt("Dont enter anything");
	console.log(sampleNullPrompt);


