//[section] mongoDB aggragation

//used to generate manipulated data and perform operations
//to create a filtered results that helps analyzing data
//- compre to doing the CRUD operations on our  data from  previous session,
//agression gives us access to manipulate , filter and compute for results
//providing us with information to make becessary development decissions wihtou having to create a frontend application

//using aggregate method

db.fruits.aggregate([
{$match : { onSale: true}},
{$group : {_id : "$supplier_id", total : {
	$sum: "$stock"
}}}
	]);

db.fruits.aggregate([
{$match : {onSale: true}}
	]); //kay match operator kung anong nakadeclare na field magmamatch , documents lang na marereturn onSale

//the group operators, groups a documents in terms of the property declared in the _id
db.fruits.aggregate([
	{$group : {_id : "$supplier_id", total : {
		$sum: "$stock"
	}}}
	]);

//max operator

db.fruits.aggregate([
  { $match: { onSale: true } },
  {
    $group: {
      _id: "$supplier_id",
      max: { $max: "$stock" },
      sum: { $sum: "$stock" }
    }
  }
]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  {
    $group: {
      _id: '$color',
      max: { $max: "$stock" },
      sum: { $sum: "$stock" }
    }
  }
]);

//field projection with aggragation
/*
The $project operator can be used when aggregating data to exclude the returned result

*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  {
    $group: {
      _id: '$color',
      max: { $max: "$stock" },
      sum: { $sum: "$stock" }
    }
  },
  {$project {_id : 0}} //use case of this operator to exclude id
]);

//sorting aggregated result 
/*
	the sort operator can be use to change the order of aggregated results 
	providing the value of -1 will sort the aggegated in a reverse order

Syntax:
- {$sort : {field : 1/ -1}}
*/
db.fruits.aggregate([
{$match : {onSale:true}},
{$group : {
	_id: "$supplier_id",
	total : { $sum : "$stock"}
}},
{$sort : {total : 1}}

])

db.fruits.aggregate([
{$match : {onSale:true}},
{$group : {
	_id: "$name",
	stocks : {$sum : "$stock"}
}},
{$sort : {_id : 1}}

])

db.fruits.aggregate([
	
	{ $group : {
		_id : "$color",
		stocks : {$sum : "$stock"}
	}},
	{ $project : { _id : 0}},
	{
		$sort : { _id : -1 }
	}
	])

	//aggegating results based on an array field
//unwind operator
/*
 the $unwind operator deconstructs an array field from a collection/field with an array values
 to output a result for each element
 synatx :
 {$unwind : field}
*/

db.fruits.aggregate([	
{$unwind : "$origin"}
]);

// display fruits documents by their origin and the kinds of fruits taht are supplied

db.fruits.aggregate([	
	{$unwind : "$origin"},
	{$group : {
		_id : "$origin",
		kinds : { $sum : 1}
	}},
	{$sort : {kinds : 1 }} 
	])

db.fruits.aggregate([
		{$unwind : "$origin"},
		{$group : {
			_id : "$origin",
			kinds: { $sum: 1}
		}}
	])

