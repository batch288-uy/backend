// console.log("Hello World");

// An array in programming is simply a list of data that shares the same data type
/**/

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

//now with an array we can simple write the code above like this:

let studentNumbers = ['2020-1923','2020-1924','2020-1925','2020-1926','2020-1927'];

//[Section]Arrays
//Arrays are use to store multiple related values in a single variable
//They are declared using the square bracket [] also know as array literals'
/*
Syntax:
let/const arrayName = [elementA,elementB,elementC ...]
*/

let grades = [98.5,94.3,89.2,90.1]; /*Numbers*/
let computerBrands = ['Acer','Asus',"Lenovo",'Neo','Redfoc','Toshiba']; /*string*/

//possible use of an array but is not recommend

let mixedArr = [12, 'Asus', null, undefined, {}];
console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

//Alternative to write arrays

let myTask = [
		'drink html',
		'eat javascript',
		'inhale css',
		'bake sss'
	]

console.log(myTask);

//Creating an array with values from variable

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1,city2,city3];
console.log(cities);

//[Section] length property
// The length property allows us to get and set the total number of items in array

console.log(myTask.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// .length property can also set the total number of element in an array, meaning we can simply actually delete the last item in the array or shorten the array by imple updating the length of an array

console.log(myTask);

// myTask.length = myTask.length -1;
myTask.length -= 2;
console.log(myTask);
console.log(myTask.length);

//todelete a specific item in an array we can employ array methods (which will be shown/discused in the next session)or algorithm

//Another example using decrementaation:
console.log(cities);
cities.length--;
console.log(cities);

// we can't do the same on strings
let fullName = "Jamie Noble";
console.log(fullName);
fullName.legnth -=1;
console.log(fullName);
//if you can shorten the array by setting the length property, you cannot shoprten the number of characters using the same property

//since you can shorten the array by setting the legnght property , you can also lengthen it by adding a number into the legnth property , Since we lenmgthen the array forcibly, there will be another item n the array , however it will be empty or undefined

let theBeatles = ["john","Ringo","Paul","George"];
console.log(theBeatles);
theBeatles.length += 1;
console.log(theBeatles);

//[Section]Reading from Arrays 
/*
Accessing array elements is one of the more common tasks that we can do with an array
we can do this by using array indexes
each element in an array is associated with its own index /nmumber
Synatx:
	arrayName[index]
*/

console.log(grades[0]);
console.log(computerBrands[2]);
console.log(theBeatles[4]);
console.log(grades[4]); /*pagwala ung tinartget na index sa elemnt undefine*/

let lakersLegends = ["Kobe","Shaq","Lebron","Magic","Kareem"];
console.log(lakersLegends);
// you can actually save/store array items in another variable

let currentLaker = lakersLegends[2];
console.log(currentLaker);

//you cana ctually reassign array values using items' indices
console.log("Array before reassignment");
console.log(lakersLegends);

//Reassignment the value of specific elemnts
lakersLegends[1] = "Pau Gasol";
console.log("Array after reassignment:");
console.log(lakersLegends);

//accessing the last elemnt of an array
//Since the first elemnt of an array starts at 0 substracting 1 to the length of an array will offset the value by one allowing us to get the last element
let bullsLegends = ["Jordan","Pippen","Rodman","Rose","Kukoc"];
console.log(bullsLegends[bullsLegends.length -1]);


//adding elements into the array 

let newArr = [];
console.log(newArr);

newArr[newArr.length] = "Cloud Strife";
console.log(newArr);

newArr[newArr.length] = "Tifa Lockhart";
console.log(newArr);

//[Section] looping over an array
// you can use for loop to iterate over all items in array
console.log("________________________________________________");
//for loop
for(let index = 0; index < bullsLegends.length; index++)
{

	console.log(bullsLegends[index]);
}

//Example
//we are going to create a lopp that willl check whether the element inside our arrat is divisible by 5 or not 
//if divisible by 5 console number + "is divisible by 5"
//if not console number + "is not divisible by 5"

let numArr = [5,12,30,46,40];

let numDivBy5 = [];
let numNotDivBy5 =[];

for(let index = 0; index < numArr.length; index ++)
{
	if (numArr[index] % 5 === 0) {
		console.log(numArr[index] + ' is divisible by 5');

		numDivBy5[numDivBy5.length] = numArr[index]
		 }

	else {
		 console.log(numArr[index] + ' is not divisible by 5');

		 numNotDivBy5[numNotDivBy5.length] = numArr[index]

		 }
}

console.log(numDivBy5);
console.log(numNotDivBy5);


//[Section]Multi Dimensional Arrays
// Multidimensional arrays are useful for storing complex data structure
//though useful in numbe rof cases ,creating complex array structure is not always recommended

// console.log("________________________________________________");

let chessboard = [['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
                  ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
                  ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
                  ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
                  ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
                  ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
                  ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
                  ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']];

console.log(chessboard);

console.log(chessboard[0][2]);
