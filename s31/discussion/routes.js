const http = require('http');

//create a variabl to store the port number
const port = 8888;


const server = http.createServer((request, response) => {

	if(request.url == '/greetings'){
		response.writeHead(200,{'Content-Type': 'text/plain'})

		response.end('Hellow michael jordan!')
	} else if (request.url == '/homepage'){
		response.writeHead(200,{'Content-Type':'text/plain'})
		
		response.end('This is Homepage')
	} else{
		response.writeHead(404,{'Content-Type':'text/plain'})

		response.end('Hey page is not available')
	}
})
server.listen(port)

console.log(`Server is now accesible at localhost:${port}.`)