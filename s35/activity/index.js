const express = require('express');
const mongoose = require('mongoose');
//mongoose is a package that allows creation schemas to model our data structures
//also has an access to a number of methods for manipulating our databases

const port = 3001;

const app = express();

//section: MongoDB Connection
//connect to the database by passing your connection string
//Duw to update in mongo db drivers, that allow connection , the default collection string is being flagged as an error
// by default a warning will be displayed in the terminal when the application is running //{newUrlParser:true}
//Syntax:
// mongoose.connect("MongoDB string",{useNewUrlParser: true});

mongoose.connect("mongodb+srv://admin:admin@batch288uy.vm2f3w2.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser:true});

//notification whether that we connected properly with the database
let db =mongoose.connection;

//for catching the rror just in case we have error during the connection

//console.error.bind allows us to print error in the browser and in the terminal

db.on("error",console.error.bind(console,"Error ! can't connect to the Database"));
// if the connection is sucesful
db.once("open", () => console.log("We're connected to the cloud database!"));

//[Section] Mongoose Schema

//schemas determine the structure of the document to be written in the database 
//schemas act as blueprint to our data
// use the schema() constructor of the mongoos module to create a new schema object

// middlewares

//allows our app to read json data
app.use(express.json());
//allows our app to read data from forms
app.use(express.urlencoded({extended: true}))


const taskSchema = new mongoose.Schema({
	//define the field with the corresponding data type// blueprint

	name: String,
	//let us add another field whioch is status
	status: {
		type: String,
	default: "pending"
	}
})

//[Section] Models
//uses schema and are used to create/instantiate objects that corresponds to the schema
// models use schema and they act as the middleman from the server (js code) to our database
//to create a model we are going to use the model()

const Task = mongoose.model('Task',taskSchema);

//Section: Routes

//create a POST route to create a new task
//create a new task
//business Logic
	//1. Add a functionality to check whether there are duplicate tasks
		//if the tasks is existing in the database , we return an error
		//if the task doesnt exist in the databse , we add it in the database
	//2. the task data will be comming from the request body
//// Task.findOne({})



app.post("/tasks", (request,response) => {
	//findOne" method is a mongoose method that acts similar to find ' in mongoDB '
//if the findOne finsd a document that matches the criteria it will return the object/document and if theres no object that matches the criteria it will return an empty object or null
	console.log(request.body)
	Task.findOne({name:request.body.name })
	.then(result => {
		//we can use if statement to checl or verify whethere we have an object found
		if(result !== null)
		{
			return response.send("Duplicate Task Found!")
		}
		else{
			//create a new task and save it to the database
			let newTask = new Task({
				name: request.body.name
			})
			//the save() method will store the information to the datbase 
			//since the new task was created from the mogoose schema and task model it will be save in tasks collection
			newTask.save()

			return response.send('New Task created!')
		}
	})
})

//get all the task in our collection 
//retrieve all the documents
//if an error is encountered, print the error
//if no errors are found send a success status tot he client and show the documents retrieved 

app.get("/tasks", (request, response) => {
	// find method is a mongoose method that is similar to mongoDB find
	//Get all ducuments
	Task.find({}).then(result =>{
		return response.send(result);
	}).catch(error => response.send(error));
})


//Activity part


const newSchema = new mongoose.Schema({
  username: String,
  password: String
});

const users = mongoose.model('Users', newSchema);

app.post("/signup", (request, response) => {
  console.log(request.body);
  users.findOne({ username: request.body.username })
    .then(result => {
      if (result !== null) {
        return response.send("Duplicate username Found!");
      } else {
        if (request.body.username && request.body.password) {
          let newUser = new users({
            username: request.body.username,
            password: request.body.password
          });

          newUser.save()
            .then(() => {
              response.status(201).send("New user registered");
            })
            .catch(error => {
              response.send(error);
            });
        } else {
          response.send("BOTH username and password must be provided");
        }
      }
    })
    .catch(error => {
      response.send(error);
    });
});


if(require.main === module){
	app.listen(port, () =>{ console.log(`Server running at port ${port}`)
})	
}
module.exports =app;
